export const quiz = [
    {
        id:1,
        question: "Question 1",
        options: 
        [
            {
                title: 'opsi 1',
                correct: true
            },
            {
                title: 'opsi 2',
                correct: false
            },
            {
                title: 'opsi 3',
                correct: false
            }
        ]
    },
    {
        id:2,
        question: "Question 2",
        options: 
        [
            {
                title: 'opsi 1',
                correct: true
            },
            {
                title: 'opsi 2',
                correct: false
            },
            {
                title: 'opsi 3',
                correct: false
            }
        ]
    },
    {
        id:3,
        question: "Question 3",
        options: 
        [
            {
                title: 'opsi 1',
                correct: true
            },
            {
                title: 'opsi 2',
                correct: false
            },
            {
                title: 'opsi 3',
                correct: false
            }
        ]
    },
    {
        id:4,
        question: "Question 4",
        options: 
        [
            {
                title: 'opsi 1',
                correct: true
            },
            {
                title: 'opsi 2',
                correct: false
            },
            {
                title: 'opsi 3',
                correct: false
            }
        ]
    }
]